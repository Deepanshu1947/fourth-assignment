package com.deepanshu.entities;

import java.util.Comparator;

/**
 * Created by Deepanshu on 9/11/2015.
 */
public class StudentFNameComparator implements Comparator<Student>{
    @Override
    public int compare(Student lhs, Student rhs) {
        return lhs.getFName().compareToIgnoreCase(rhs.getFName());
    }
}
