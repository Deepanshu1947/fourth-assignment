package com.deepanshu.entities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import static com.deepanshu.firstapp.Welcome.stuDataBase;

import com.deepanshu.firstapp.R;

import static com.deepanshu.firstapp.Welcome.idCount;

public class DataAdapter extends BaseAdapter {
    Context context;
    TextView idTV,nameTV;

    public DataAdapter(Context context) {
        this.context = context;

    }

    @Override
    public int getCount() {
        return stuDataBase.size();
    }

    @Override
    public Student getItem(int position) {

        return stuDataBase.get(position);
    }

    @Override
    public long getItemId(int position) {
        return stuDataBase.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater li = LayoutInflater.from(context);
            convertView = li.inflate(R.layout.block_layout, parent, false);
            idTV=(TextView) convertView.findViewById(R.id.label_id_show);
            nameTV=(TextView) convertView.findViewById(R.id.label_name_show);
            Student s=new Student();
            s=getItem(position);
            idTV.setText(String.valueOf(s.getId()));
            nameTV.setText(s.getFName());
        return convertView;
    }
}
