package com.deepanshu.entities;

import com.deepanshu.util.Conventions;

import java.util.Comparator;

/**
 * Created by Deepanshu on 9/11/2015.
 */
public class StudentIdComparator implements Conventions,Comparator<Student> {
    @Override
    public int compare(Student lhs, Student rhs) {
        return lhs.getId()<rhs.getId()?FALSE:TRUE;
    }
}
