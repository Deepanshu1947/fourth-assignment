package com.deepanshu.firstapp;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.deepanshu.entities.DataAdapter;
import com.deepanshu.entities.Student;
import com.deepanshu.entities.StudentFNameComparator;
import com.deepanshu.entities.StudentIdComparator;
import com.deepanshu.util.Conventions;

import java.util.ArrayList;
import java.util.Collections;

public class Welcome extends AppCompatActivity implements Conventions, View.OnClickListener {
    Button listView, gridView, addData;
    GridView gridData;
    ListView listData;
    public static int position = 0, idCount = 1;
    public static ArrayList<Student> stuDataBase = new ArrayList<>();
    Intent stuPage;
    DataAdapter viewDataAdap;
    Dialog dialogOperations;
    Spinner sortSpinner;
    ArrayAdapter<String> sortByAdap;
    int sortBy=SORT_BY_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        init();
        setOnClick();

    }

    public void showDialog() {
        dialogOperations = new Dialog(Welcome.this);
        dialogOperations.setContentView(R.layout.dialog_layout);
        Button viewBT = (Button) dialogOperations.findViewById(R.id.viewBT);
        Button updateBT = (Button) dialogOperations.findViewById(R.id.updateBT);
        Button deleteBT = (Button) dialogOperations.findViewById(R.id.deleteBT);
        dialogOperations.setTitle("Operations");
        updateBT.setOnClickListener(Welcome.this);
        deleteBT.setOnClickListener(Welcome.this);
        viewBT.setOnClickListener(Welcome.this);

        dialogOperations.show();

    }

    public void init() {
        gridData = (GridView) findViewById(R.id.gridData);
        listData = (ListView) findViewById(R.id.listData);
        listView = (Button) findViewById(R.id.listView);
        gridView = (Button) findViewById(R.id.gridView);
        addData = (Button) findViewById(R.id.addData);
        viewDataAdap = new DataAdapter(this);
        showData(LIST_VIEW);
        stuPage = new Intent(Welcome.this, AddStudent.class);
        sortSpinner = (Spinner) findViewById(R.id.sortBySpinner);
        sortByAdap = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.sortby));
        sortSpinner.setAdapter(sortByAdap);

    }

    public void setOnClick() {

        listView.setOnClickListener(Welcome.this);
        gridView.setOnClickListener(Welcome.this);
        addData.setOnClickListener(Welcome.this);

        gridData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
                Welcome.position = itemPosition;
                showDialog();
            }
        });
        listData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
                Welcome.position = itemPosition;
                showDialog();
            }
        });
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int sortPosition, long id) {
                sortBy=sortPosition;
                sortList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void sortList() {
        sortSpinner.setSelection(sortBy);

        switch (sortBy) {
            case SORT_BY_ID:
                Collections.sort(stuDataBase, new StudentIdComparator());
                break;
            case SORT_BY_NAME:
                Collections.sort(stuDataBase, new StudentFNameComparator());
                break;
        }
        viewDataAdap.notifyDataSetChanged();

    }

    public void showData(int choice) {
        switch (choice) {
            case GRID_VIEW:
                listView.setFocusableInTouchMode(false);
                gridView.setFocusableInTouchMode(true);
                gridData.setVisibility(View.VISIBLE);
                listData.setVisibility(View.GONE);
                gridData.setAdapter(viewDataAdap);
                break;
            case LIST_VIEW:
                gridData.setVisibility(View.GONE);
                listData.setVisibility(View.VISIBLE);
                listView.setFocusableInTouchMode(true);
                gridView.setFocusableInTouchMode(false);
                listData.setAdapter(viewDataAdap);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.listView:
                showData(LIST_VIEW);
                break;
            case R.id.gridView:
                showData(GRID_VIEW);
                break;
            case R.id.adddata:
                stuPage.putExtra("toDo", ADD_STUDENT);
                startActivityForResult(stuPage, MAIN_PAGE);

                break;
            case R.id.viewBT:
                dialogOperations.dismiss();
                stuPage.putExtra("toDo", DIALOG_VIEW);
                stuPage.putExtra("position",Welcome.position);
                startActivityForResult(stuPage, DIALOG_BOX);
                break;
            case R.id.updateBT:
                dialogOperations.dismiss();
                stuPage.putExtra("position",Welcome.position);
                stuPage.putExtra("toDo", DIALOG_EDIT);
                startActivityForResult(stuPage, DIALOG_BOX);

                break;
            case R.id.deleteBT:
                dialogOperations.dismiss();
                stuDataBase.remove(position);
                viewDataAdap.notifyDataSetChanged();
                showToast(STU_REMOVED);
                break;

            default:
                break;


        }

    }

    public void showToast(String data) {
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Student tempStu = new Student();
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ADD_STUDENT:
                    tempStu.setAddress(data.getStringExtra("address"));
                    tempStu.setId(idCount++);
                    tempStu.setFName(data.getStringExtra("fName"));
                    tempStu.setLName(data.getStringExtra("lName"));
                    tempStu.setMobileNumber(data.getStringExtra("mobileNo"));
                    stuDataBase.add(tempStu);
                    showToast(STU_ADDED);
                    break;
                case DIALOG_BOX:
                    showToast(STU_UPDATED);
                    break;

                default:
                    break;
            }
        }
        sortList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
