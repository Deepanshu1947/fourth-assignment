package com.deepanshu.firstapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.deepanshu.entities.Student;
import com.deepanshu.util.Conventions;
import com.deepanshu.util.Validations;

import static com.deepanshu.firstapp.Welcome.idCount;
import static com.deepanshu.firstapp.Welcome.position;
import static com.deepanshu.firstapp.Welcome.stuDataBase;
import static com.deepanshu.util.Conventions.ADD_STUDENT;
import static com.deepanshu.util.Conventions.DIALOG_EDIT;
import static com.deepanshu.util.Conventions.DIALOG_VIEW;

public class AddStudent extends AppCompatActivity implements View.OnClickListener, Conventions {
    EditText firstNameEdit, lastNameEdit, addressEdit, mobileNoEdit;
    TextView setIdTV;
    Button saveButton, cancelButton;
    Intent currIntent;
    Student tempStu;
    int choice;
    Validations check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
    }

    public void init() {
        setIdTV = (TextView) findViewById(R.id.setIdEdit);
        firstNameEdit = (EditText) findViewById(R.id.firstNameEdit);
        lastNameEdit = (EditText) findViewById(R.id.lastNameEdit);
        addressEdit = (EditText) findViewById(R.id.addressEdit);
        mobileNoEdit = (EditText) findViewById(R.id.mobNoEdit);
        cancelButton = (Button) findViewById(R.id.cancelButton);
        saveButton = (Button) findViewById(R.id.saveButton);
        currIntent = getIntent();
        choice = currIntent.getIntExtra("toDo", DEFAULT_USER);
check=new Validations();

    }

    public void setOnClick() {
        cancelButton.setOnClickListener(this);
        switch (choice) {
            case ADD_STUDENT:
                setIdTV.setText(String.valueOf(idCount));
                saveButton.setOnClickListener(this);
                saveButton.setText(SAVE);
                break;
            case DIALOG_EDIT:
                viewStudent();
                saveButton.setText(UPDATE);
                saveButton.setOnClickListener(this);
                break;
            case DIALOG_VIEW:
                setNonEditable();
                viewStudent();
                break;

        }
    }

    public  void viewStudent()
    {
       tempStu = stuDataBase.get(currIntent.getIntExtra("position", 0));
        setIdTV.setText(String.valueOf(tempStu.getId()));
        firstNameEdit.setText(tempStu.getFName());
        lastNameEdit.setText(tempStu.getLName());
        addressEdit.setText(tempStu.getAddress());
        mobileNoEdit.setText(tempStu.getMobileNumber());

    }
    public void setNonEditable() {
        saveButton.setVisibility(View.GONE);
        firstNameEdit.setKeyListener(null);
        lastNameEdit.setKeyListener(null);
        addressEdit.setKeyListener(null);
        mobileNoEdit.setKeyListener(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Student getDataFromScreen()
    {
        Student newStudent = new Student();
        newStudent.setId(Integer.parseInt((String) setIdTV.getText()));
        newStudent.setFName(firstNameEdit.getText().toString());
        newStudent.setLName(lastNameEdit.getText().toString());
        newStudent.setAddress(addressEdit.getText().toString());
        newStudent.setMobileNumber(mobileNoEdit.getText().toString());
return newStudent;
    }

    @Override
    public void onClick(View v) {
        Student newStudent=getDataFromScreen();
        switch (v.getId()) {
            case R.id.saveButton:
                if (check.isNonEmptyStudent(newStudent))
                {
                    if (saveButton.getText().equals("UPDATE")) {
                        stuDataBase.set(position, newStudent);
                        setResult(RESULT_OK);
                        finish();
                    } else if (saveButton.getText().equals("SAVE")) {
                        Intent tempIntent = new Intent();
                        tempIntent.putExtra("fName", newStudent.getFName());
                        tempIntent.putExtra("lName", newStudent.getLName());
                        tempIntent.putExtra("address", newStudent.getAddress());
                        tempIntent.putExtra("mobileNo", newStudent.getMobileNumber());
                        setResult(RESULT_OK, tempIntent);
                        finish();
                    }
                } else {
                    Toast.makeText(this,INSUFFICIENT_INFO, Toast.LENGTH_LONG).show();
                    setResult(RESULT_CANCELED);
                }
                break;

            case R.id.cancelButton:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }

    }
}